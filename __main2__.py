import shutil

from csv_functions import create_csv, write_line, save_book_image
from page_functions import get_all_categories_link, get_all_books_url_from_category, extract_book_infos

if __name__ == '__main__':
    shutil.rmtree('data/', ignore_errors=True)
    categories = get_all_categories_link()

    for category in categories:
        category_name = category.split("/")[-2]
        category_name = category_name.split("_")[0].title()

       
        create_csv(category_name)
        books = get_all_books_url_from_category(category)
         for book in books:
            book_infos = extract_book_infos(book)
            write_line(category_name, book_infos)
            save_book_image(book_infos['image_link'], category_name, book_infos['upc'])

