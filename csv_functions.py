import csv
import os
import requests


def create_csv(title: str) -> None:
    """
       Create the folders and files used for saving data and write CSV headers

        Args:
            title: category's name

       Returns:
           No return
       """
    os.makedirs(f"data/{title}/images/")
    column_title = [
        "product_page_url",
        "universal_product_code",
        "title",
        "price_including_tax",
        "price_excluding_tax",
        "number_avalaible",
        "product_description",
        "category",
        "review_rating",
        "image_url"
    ]
    with open(f"data/{title}/Infos_{title}.csv",
              "w", encoding="utf-8", newline='') as new_file:
        csv_writer = csv.writer(new_file)
        csv_writer.writerow(column_title)


def write_line(title: str, book_info: dict) -> None:
    """
       Add a line to a category CSV with the information of one book

        Args:
            title: category's name,
            book_info: dictionnary of book's data

       Returns:
           No return
       """
    with open(f"data/{title}/Infos_{title}.csv",
              "a", encoding="utf-8", newline='') as file:
        csv_writer = csv.writer(file)
        csv_writer.writerow(
            [
                book_info["url"],
                book_info["upc"],
                book_info["title"],
                book_info["price_inc_tax"],
                book_info["price_exc_tax"],
                book_info["availability"],
                book_info["description"],
                book_info["category"],
                book_info["rating"],
                book_info["image_link"]
            ]
        )


def save_book_image(image_url: str, category_name: str, book_upc: str) -> None:
    """
       Download the cover image of a book

        Args:
            image_url: image's URL,
            category_name: name of the book's category,
            book_upc: book's code to rename the image

       Returns:
           No return
       """
    page = requests.get(image_url)
    if page.ok:
        file_extention = os.path.splitext(image_url)[-1]
        new_file = open("data/" + category_name + "/images/" + book_upc + file_extention, 'wb')
        new_file.write(page.content)
        new_file.close()
