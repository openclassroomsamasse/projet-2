# Book Scrapper

Book scrapper is a [*OpenClassrooms*](https://openclassrooms.com) project. 
The objective of this program is to extract information about all the books on [BooksToScrape](https://books.toscrape.com/) and save it in a CSV file, as well as downloading the cover image.

## Installation

Clone the package, create your own virtual environment and install the requirements to run this app.

```bash
git clone https://gitlab.com/openclassroomsamasse/projet-2.git
pipenv shell
pip install -r requirements.txt
```

## Usage

To run the app, launch the main file.

```bash
python -m __main__.py
```

## Notes

The application can take up to **an hour** to run depending on your internet connection and computer.

During running a folder data will be created.

The file __main2__.py is the same file than main without the printing system

## License

[MIT](https://choosealicense.com/licenses/mit/)
