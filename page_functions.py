import re

import requests
from bs4 import BeautifulSoup

from constants import BASE_URL, RATINGVALUES


def get_all_categories_link() -> list:
    """
    Return a list of absolute URLs of categories

    Returns:
        list of strings: URLs of categories.
    """
    html_page = requests.get(BASE_URL)
    if html_page.ok:
        soup_object = BeautifulSoup(html_page.content, 'html.parser')
        links_category = soup_object.aside.ul.ul.find_all("a")
        links_list = []
        for link in links_category:
            links_list.append(BASE_URL + link['href'])
        return links_list
    else:
        print("Une erreur s'est produite en essayant d'acceder a la page")


def get_all_books_url_from_category(category_url: str) -> list:
    """
    Return a list of absolute URLs of books for a specific category

    Returns:
        list of strings: URLs of books.
    """

    # URL format for the first one is different
    urls_list = []
    urls_list += get_books_url_from_page(category_url)

    # Other pages follow the same scheme
    page_number = 2
    page_exists = True
    while page_exists:
        page_url_list = get_books_url_from_page(
            category_url.replace('index.html', 'page-' + str(page_number) + '.html')
        )
        if page_url_list:
            urls_list += page_url_list
            page_number += 1
        else:
            page_exists = False

    return urls_list


def get_books_url_from_page(page_url: str) -> list:
    """
    Return a list of books for a specific page of a category

    Args:
        page_url: url of the page to parse

    Returns:
        list of strings: URLs of books.
    """
    page_category_html = requests.get(page_url)

    books_links = []
    if page_category_html.ok:
        page_category_soup = BeautifulSoup(
            page_category_html.content, 'html.parser')
        li_list = page_category_soup.section.find_all('article')
        books_links = [
            link.replace("../../..", BASE_URL + "catalogue") for link in [
                li.select_one('a')['href'] for li in li_list
            ]
        ]
    return books_links


def extract_book_infos(book_url: str) -> dict:
    """
    Return a dict of book data

    Args:
        book_url: url of the page to parse

    Returns:
        dict: data about the book.
    """
    page_book = requests.get(book_url)
    if page_book.ok:
        soup_object = BeautifulSoup(page_book.content, 'html.parser')
        book_info = {}

        """ Prepare infos from table """
        table_key = soup_object.table.find_all("th")
        table_key = [key.text for key in table_key]
        table_value = soup_object.table.find_all("td")
        table_value = [value.text for value in table_value]
        product_info = dict(zip(table_key, table_value))

        """ Get title """
        try:
            book_info['title'] = soup_object.article.h1.text
        except AttributeError:
            book_info['title'] = "No title available"

        """ Get description """
        try:
            book_info['description'] = soup_object.article.find(id="product_description").next_sibling.next_sibling\
                .text.replace(' ...more', '')
        except AttributeError:
            book_info['description'] = "No description available"

        """ Get price including tax """
        try:
            book_info['price_inc_tax'] = product_info["Price (incl. tax)"].replace("£", "")
        except (AttributeError, KeyError):
            book_info['price_inc_tax'] = "No price inc. tax available"

        """ Get price excluding tax """
        try:
            book_info['price_exc_tax'] = product_info["Price (excl. tax)"].replace("£", "")
        except (AttributeError, KeyError):
            book_info['price_exc_tax'] = "No price excl. tax available"

        """ Get availability """
        try:
            book_info['availability'] = re.search("[0-9]+", product_info["Availability"]).group()
        except (AttributeError, KeyError):
            book_info['availability'] = "No availability info available"

        """ Get rating """
        try:
            book_info['rating'] = RATINGVALUES[soup_object.select_one(".star-rating")['class'][1]]
        except (AttributeError, KeyError, ValueError):
            book_info['rating'] = "No rating available"

        """ Get image link """
        try:
            book_info['image_link'] = soup_object.select_one(".carousel-inner img")['src'].replace("../..", BASE_URL)
        except (AttributeError, KeyError):
            book_info['image_link'] = "No image available"

        """ Get category """
        try:
            book_info['category'] = soup_object.select('ul.breadcrumb > li')[-2].text.replace("\n", "")
        except (AttributeError, KeyError):
            book_info['category'] = "No category available"

        """ Get UPC """
        try:
            book_info['upc'] = product_info["UPC"]
        except KeyError:
            book_info['upc'] = "No UPC value available"

        """ Get book url """
        book_info['url'] = book_url

        return book_info
