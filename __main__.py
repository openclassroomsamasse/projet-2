import shutil
from rich.console import Console


from csv_functions import create_csv, write_line, save_book_image
from page_functions import get_all_categories_link, get_all_books_url_from_category, extract_book_infos

if __name__ == '__main__':
    console = Console()
    console.rule("[red]Program started[/red]")

    with console.status(
            "[bold green]Deleting original folder..."
    ):
        shutil.rmtree('data/', ignore_errors=True)
        console.print("[green]Data folder deleted[/green]")

    with console.status(
            "[bold green]Extracting categories links..."
    ):
        categories = get_all_categories_link()
        console.print("[green]Categories links extracted[/green]")

    for category in categories:
        category_name = category.split("/")[-2]
        category_name = category_name.split("_")[0].title()

        with console.status(
                f"[bold green]Creating '{category_name}' category folder and CSV file..."
        ):
            create_csv(category_name)
            console.print(
                f"[green]Categorie [/green][blue]'{category_name}'[/blue][green] folder and CSV file created[/green]"
            )

        with console.status(
                f"[bold green]Extracting [/bold green][blue]'{category_name}'[/blue][bold green] books links..."
        ):
            books = get_all_books_url_from_category(category)
            console.print(
                f"[green]Categorie [/green][blue]'{category_name}'[/blue][green] books links extracted[/green]"
            )

        with console.status(
                f"[bold green]Saving [/bold green][blue]'{category_name}'[/blue][bold green] books infos and images..."
        ):
            for book in books:
                book_infos = extract_book_infos(book)
                write_line(category_name, book_infos)
                save_book_image(book_infos['image_link'], category_name, book_infos['upc'])
            console.print(f"[green]{len(books)} books saved for category [/green][blue]'{category_name}'[/blue]")
    console.rule(f'[red]Program done')
